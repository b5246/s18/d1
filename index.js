//=============OBJECT
let cellPhone = {
	name: 'Nokia 3315',
	manufactureDate: 1999,

}
console.log("Result from creating objects using initializers/literal notation");
console.log( cellPhone);
console.log(typeof cellPhone);

function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}

// Creating unique instance of the Laptop Object
let laptop = new Laptop('Lenovo', 2008);
console.log("Result from creating objects using objects contructors");
console.log(laptop);

let myLaptop = new Laptop("Macbook Air", 2020);
console.log(myLaptop);

let oldLaptop = Laptop('Alienware', 2016);
console.log("Result from creating objects without the new keyword: ");
console.log(oldLaptop);

let computer = {};
let myComputer = new Object();

console.log("Result from dot notation: " + myLaptop.name);
console.log("Result from dot notation: " + myLaptop.manufactureDate);
console.log("Result from square bracket notation: " + laptop['name']);

let array = [laptop, myLaptop];
console.log(array[0]['name']);
console.log(array[1].name);
// =================================================================>
//FIRST: hindi pumasok sa Laptop function
console.log("BEFORE:",array);
let bagongBili = array.push({name: "Acer", manufactureDate: 2022});
//console.log(bagongBili); // show Total number of values
console.log("AFTER:",array);
//SECOND: pinapass ko saloob ni Laptop function

console.log("BEFORE:",array);
let Bibili = array.push(new Laptop("HP", 2022));
//console.log(bagongBili); // show Total number of values
console.log("AFTER:",array);

// ================ EXAMPLE (func to func access)
const rectangle = {
    radius: 10
};
const style = {
    Backcolour: 'red'
};
const solidRectangle = {
    ...rectangle,
    ...style
};
console.log(solidRectangle);
// EXPECTED OUTPUT: { radius: 10, Backcolour: 'red' }
// ==================================================================>

let car = {};
car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation.');
console.log(car);
car['manufacture date'] = 2019;
console.log(car['manufacture date']);
// console.log(car['Manufacture Date']);

console.log(car.manufactureDate);
console.log('Result from adding properties using square bracket notation:');
console.log(car);
//=============================DELETE
delete car['manufacture date'];
console.log('Result from deleting properties:');
console.log(car);
//============================REASSIGN
car.name = 'Dodge Charger R/T';
console.log('Result from reassigning properties:');
console.log(car);

//=============================OBJECT METHODS

let person = {
	name: 'John',
	talk: function(){
		console.log("Hello my name is" + this.name)
	}
}
console.log(person);
console.log("Result from object methods:");
person.talk();                    // Access function within function

// Adding methods to objects
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward.');
}
person.walk();

person.sing = function(){
	console.log(this.name + ' sang 1000 times - badly -with his kareoke.')
}
person.sing();

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		country: 'US'
	},
	emails: ['joe@mail.com','joesmaith@email.net'],
	introduce: function(){
		console.log("Hello my name is " + this.firstName + ' ' + this.lastName);
	}
}

friend.introduce();

// ====================Real world Application of objects

// Using object literal to create multiple kinds of pokemon
let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled target Pokemon");
		console.log("Target Pokemon's health is now reduce to _targetPokemonHealth_");
	},
	faint: function(){
		console.log("Pokemon Fainted");
	}
}
// Creating an object constructor to help in creating faster process
function Pokemon(name, level) {
	//Properties
	this.name = name;
	this.level = level;
	this.health = 2* level;
	this.attack = level;

	//Methods
	this.tackle = function(target){
		console.log(this.name+' tackled '+target.name);
		console.log("targetPokemon's health is now rduced to _targetPokemonHealth_")
	}
	this.faint = function(){
		console.log(this.name + 'fainted');
	}
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);































